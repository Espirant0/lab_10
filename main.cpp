﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include "circle.hpp"
#include <vector>

using namespace std::chrono_literals;
using namespace saw;



int main()
{
    //Рандомайзер
    srand(time(0));
    const int width = 800;
    const int height = 600;

    sf::RenderWindow window(sf::VideoMode(width, height), L"SFML работает!"); //  Создание окна(раземер, заголовок)
    
    const int N = 5;
    std::vector<Circle*> circles;
    //Инициализация фигур
    for (int i = 0; i <= width; i += width / N)
    {
        circles.push_back(new Circle(0, 50, 100, 3, rand()%6+1));
    }
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            //Обработка события 
            if (event.type == sf::Event::Closed)
                window.close();
        }
        //Движение фигур
        for (const auto& circle : circles) {
            circle->Move();
            if(circle->GetX()>width)
                circle->SetX(width);
        }
        window.clear();
        for(const auto& circle: circles)
            window.draw(*circle->Get());
        window.display();
        std::this_thread::sleep_for(20ms);
    }
    //Очистка
    for (const auto& circle : circles)
        delete circle;
    circles.clear();
    return 0;
}
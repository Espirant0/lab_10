#pragma once
#include <SFML/Graphics.hpp>


namespace saw {
	class Circle {
	public:
		Circle(int x, int y, float r, int k, float velocity);
		~Circle();
		sf::CircleShape* Get();
		void Move();
		void SetX(int x);
		int GetX();
		
	private:
		int m_x, m_y, m_k;
		float m_r, m_velocity;
		sf::CircleShape* m_shape;
	};
}
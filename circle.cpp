#include "circle.hpp"

namespace saw {
	Circle::Circle(int x, int y, float r, int k, float velocity)
	{
		m_x = x;
		m_y = y;
		m_r = r;
		m_k = k;
		m_velocity = velocity;
		m_shape = new sf::CircleShape(m_r, m_k);
		m_shape->setOrigin(m_r, m_k);
		m_shape->setFillColor(sf::Color::Blue);
		m_shape->setPosition(m_x, m_y);
	}
	Circle::~Circle()
	{
		delete m_shape;
	}

	sf::CircleShape* Circle::Get() { return m_shape; }

	void Circle::Move()
	{
		
		m_x += m_velocity;
		m_shape->setPosition(m_x, m_y);
	}

	void Circle::SetX(int x)
	{
		m_x = x;
		m_shape->setPosition(m_x, m_y);
	}

	int Circle::GetX() { return m_x; }

}